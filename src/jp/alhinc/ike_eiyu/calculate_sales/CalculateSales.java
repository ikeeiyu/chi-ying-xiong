package jp.alhinc.ike_eiyu.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class CalculateSales {

	public static void main(String[] args) {
        try {
            //key=コード・value=店舗名
            Map<String, String> branchMap = new HashMap<>();
            //key=コード・value=売上金
            Map<String, Long> earningMap = new HashMap<>();

        	BufferedReader br = null;
        	try {
        		File branchLst = new File(args[0], "branch.lst");

        		if (!branchLst.exists()) {
        			System.out.println("支店定義ファイルが存在しません");
        			return;
                }

        		br = new BufferedReader(new FileReader(branchLst));

        		//行でバッファを読み込み(line)→分割(branchs)→マップへ
        		String line;
        		while ((line = br.readLine()) != null) {
        			String[] branches = line.split(",");
        			if (branches.length != 2 || !branches[0].matches("[0-9]{3}") || branches[1].isEmpty()) {
        				System.out.println("支店定義のフォーマットが不正です");
        				return;
                    }

                    branchMap.put(branches[0], branches[1]);
                    earningMap.put(branches[0], 0L);
        		}
        	}
        	catch (IOException e) {
        		System.out.println("予期せぬエラーが発生しました");
        		return;
        	}
        	finally {
        		if (br != null) {
        			try {
        				br.close();
        			} 
        			catch (IOException e) {
        				System.out.println("予期せぬエラーが発生しました");
        				return;
        			}
        		}
        	}

        	//売上ファイルの読み込み
        	String path = args[0];
        	File dir = new File(path);
        	File[] files = dir.listFiles();
        	List<String> rcdFileNames = new ArrayList<>();

            //該当ファイルの検索
            for (File file : files) {
        		if (file.getName().matches("[0-9]{8}.rcd") && file.isFile()) {
        			rcdFileNames.add(file.getName());

        			//検索したファイルの読み込み→バッファへ
        			try {
        				br = new BufferedReader(new FileReader(file));

                        //支店コードが入っている（String型）
                        String code = br.readLine();
                        //売上金が入っている。（long型）
                        Long amount = Long.parseLong(br.readLine());

        				if (br.readLine() != null) {
        					System.out.println(file.getName() + "のフォーマットが不正です");
        					return;
        				}
        				if (!branchMap.containsKey(code)) {
        					System.out.println(file.getName() + "の支店コードが不正です");
        					return;
        				}
                        earningMap.put(code, earningMap.get(code) + amount);	//売上マップにキーとバリューを入れる

        				if (earningMap.get(code).toString().length() > 10) {
        					System.out.println("合計金額が10桁を超えました");
        					return;
        				}
        			}
        			catch (IOException e) {
                        System.out.println("予期せぬエラーが発生しました");
                        return;
        			}
        			finally {
        				br.close();
        			}
        		}
        	}

            Collections.sort(rcdFileNames);

	    	//連番チェックのコード
	    	for (int i = 0 ; i < rcdFileNames.size() - 1; i++) {
	    		String rcdFilename = rcdFileNames.get(i).split("\\.")[0];
	    		int nameInt = Integer.parseInt(rcdFilename);
	    		String number = String.valueOf(nameInt + 1);
	    		number = String.format("%08d", Integer.parseInt(number));

	    		if (!rcdFileNames.contains(number + ".rcd")) {
                    System.out.println("売上ファイル名が連番になっていません");
                    return;
	    		}
	    	}

	    	//集計ファイルの作成・データの出力
	    	try {
	    		File branchOut = new File(args[0], "branch.out");
	    		BufferedWriter bw = new BufferedWriter(new FileWriter(branchOut));
	    		for (Entry<String, String> entry : branchMap.entrySet()) {
                    bw.write(entry.getKey() + "," + entry.getValue() + "," + earningMap.get(entry.getKey()));
                    bw.newLine();
	    		}

	    		bw.close();
	    	} 
	    	catch (IOException e) {
        	    System.out.println("予期せぬエラーが発生しました");
	    		return;
	    	}
        }
        catch (Exception e) {
        	System.out.println("予期せぬエラーが発生しました");
        }
	}
}
